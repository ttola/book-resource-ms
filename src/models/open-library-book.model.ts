export default interface OpenLibraryBooks {
  [key: string]: OpenLibraryBook
}

interface OpenLibraryBook {
  url: string
  authors: OpenLibraryAuthor[]
  title: string
  cover: OpenLibraryCoverImage
}

interface OpenLibraryAuthor {
  name: string
  url: string
}

interface OpenLibraryCoverImage {
  small: string
  medium: string
  large: string
}
