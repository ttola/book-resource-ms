import * as bodyParser from 'body-parser'
import * as express from 'express'
import apiRouter from '../routes/index.server.route'

const app: express.Express = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use('/api/v1', apiRouter)
app.use((req: express.Request, res: express.Response, next: Function): void => {
  const err: Error = new Error('Not Found')
  next(err)
})
export default app
