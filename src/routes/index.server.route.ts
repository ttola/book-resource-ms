import { Router } from 'express'
import { bookController } from '../controllers/book.controller'
const apiRouter = Router()

apiRouter.get('/books', bookController.get)
export default apiRouter
