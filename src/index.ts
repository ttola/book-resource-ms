import * as http from 'http'
import { env } from './config/config'
// tslint:disable-next-line: no-require-imports
import app from './config/express'

const server: http.Server = new http.Server(app)
server.listen(env.port)
server.on('error', (e: Error) => {
  console.log('Error starting server' + e)
})

server.on('listening', () => {
  console.log(
    `Server started on port ${env.port} on env ${process.env.NODE_ENV || 'dev'}`
  )
})
