import { Request, Response } from 'express'
import { getBooks, filterBooks } from '../services/book.service'
import BookDto from '../dto/book.dto'
import { env } from '../config/config'

export default class BookController {
  public async get(req: Request, res: Response, next: Function) {
    const { q } = req.query
    if (q && q.length < 2) {
      return res.status(400).json({
        status: 400,
        message: 'Min of 2 chars in param "q" is required for filtering',
      })
    }

    try {
      const { featuredBooks } = env
      let books: BookDto[] = []
      if (q && q.length) {
        books = await filterBooks(q, featuredBooks)
      } else {
        books = await getBooks(featuredBooks)
      }
      // console.log('Books: ', books)
      res.status(200).json(books)
    } catch (err) {
      res.status(err.status || 500).json({ moreInformation: err })
    }
  }
}

export const bookController = new BookController()
