import { env } from '../config/config'
import * as superagent from 'superagent'
import BookDto from '../dto/book.dto'
import OpenLibraryBooks from '../models/open-library-book.model'
console.log('Calling OL HOST:', env.openLibraryAPIHost)
/**
 * Service operation to retrieve list of books from API
 */

export const getBooks = async (
  query: {
    [key: string]: any
  } = {}
): Promise<BookDto[]> => {
  const queryParam = Object.assign(
    {
      jscmd: 'data',
      format: 'json',
    },
    query
  )
  const res = await superagent
    .get(`${env.openLibraryAPIHost}/books`)
    .query(queryParam)
  const olBooks: OpenLibraryBooks = res.body
  return Object.entries(olBooks).map(([OLID, olBook]) => {
    const { cover, authors, title } = olBook
    return {
      coverImage: cover && cover.medium,
      title,
      authors: authors && authors.map(m => m.name),
      OLID,
      price: 0,
    }
  })
}

export const filterBooks = async (
  searchText: string,
  presetQuery: {
    [key: string]: any
  } = {}
) => {
  if (searchText.startsWith('OLID')) {
    return await getBooks({ bibkeys: searchText })
  }
  const books = await getBooks(presetQuery)
  return books.filter(
    b =>
      b.title.toLowerCase().includes(searchText.toLowerCase()) ||
      b.OLID.includes(searchText)
  )
}
