export default interface BookDto {
  coverImage: string
  title: string
  authors: string[]
  OLID: string
  price: number
}
