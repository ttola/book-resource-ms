import * as nock from 'nock'
import * as querystring from 'querystring'
import { presetBooks } from './data'

const stubHost = `http://test`
export const bookInterceptor = () => {
  return nock(stubHost)
    .persist()
    .get('/books')
    .query(true)
    .reply(uri => {
      const [, rawQuery] = uri.split('?')
      const query = querystring.parse(rawQuery)
      const { jscmd, bibkeys, format } = query as any
      let code
      let body
      if (!jscmd || !bibkeys || !format) {
        return [400, {}]
      }
      if (jscmd !== 'data' || !bibkeys || format !== 'json') {
        return [422, {}]
      }
      const bibKeysList = bibkeys.split(',')
      const matches = {}
      bibKeysList
        .filter(key => !!presetBooks[key])
        .forEach(key => {
          presetBooks[key].title = `MOCK-${presetBooks[key].title}`
          matches[key] = presetBooks[key]
        })
      if (Object.entries(matches).length === 0) {
        return [404, {}]
      }
      return [200, matches]
    })
}
