import { expect, assert } from 'chai'
import * as nock from 'nock'
import { after, afterEach, before, beforeEach } from 'mocha'

import { bookInterceptor } from './fixtures/book.fixture'
import app from '../src/config/express'
import { presetBooks } from './fixtures/data'
import { env } from '../src/config/config'
import * as sinon from 'sinon'
const request = require('supertest')

describe('Books:', function() {
  before(() => {
    bookInterceptor()
  })

  after(() => {
    nock.restore()
  })
  it('Should return all expected featured books', async () => {
    return await request(app)
      .get('/api/v1/books/')
      .expect(200)
      .then(res => {
        expect(res.body.length).equal(14)
        expect(res.body[0].OLID).not.to.be.null
      })
  })
  it('Should return 1 book for a 1 valid OLID code search query', async () => {
    const OLID = 'OL24347578M'
    return await request(app)
      .get('/api/v1/books/')
      .query({ q: OLID })
      .expect(200)
      .then(res => {
        expect(res.body.length).equal(1)
        expect(res.body[0].OLID).equal('OLID:' + OLID)
      })
  })
  it('Should return 1 book for a 1 valid OLID search query', async () => {
    const OLID = 'OLID:OL24347578M'
    return await request(app)
      .get('/api/v1/books/')
      .query({ q: OLID })
      .expect(200)
      .then(res => {
        expect(res.body.length).equal(1)
        expect(res.body[0].OLID).equal(OLID)
      })
  })
  it('Should return 1 book for a 1 valid full title search query', async () => {
    const query = 'A farewell to arms'
    return await request(app)
      .get('/api/v1/books/')
      .query({ q: query })
      .expect(200)
      .then(res => {
        expect(res.body.length).equal(1)
        expect(res.body[0].title).contains(query)
      })
  })
  it('Should return 1 book for a 1 valid partial title search query', async () => {
    const query = 'A farewell to a'
    return await request(app)
      .get('/api/v1/books/')
      .query({ q: query })
      .expect(200)
      .then(res => {
        expect(res.body.length).equal(1)
        expect(res.body[0].title).contains(query)
      })
  })
  it('Should return all matching books for a partial title search query', async () => {
    const query = 'The'
    return await request(app)
      .get('/api/v1/books/')
      .query({ q: query })
      .expect(200)
      .then(res => {
        expect(res.body.length).equal(7)
        expect(res.body[0].title).contains(query)
      })
  })
  it('Should return bad request when search query < 2 chars', async () => {
    const query = 'T'
    return await request(app)
      .get('/api/v1/books/')
      .query({ q: query })
      .expect(400)
  })
  it('Should return not found for an invalid search query', async () => {
    const query = 'OLID:invalid'
    return await request(app)
      .get('/api/v1/books/')
      .query({ q: query })
      .expect(404)
  })
})
