# Salestrip openlibrary book service

based on nodejs template: `https://github.com/rjmacarthy/express-typescript-starter.git`

# Installation

Clone the repository

```
npm install
```

Starting

```
npm start
```

Test

```
npm test
```

Build to `./dist`

```
npm run build
```

Browse to http://localhost:3000

# Usage

Get all featured books

```
curl -X GET \
  'http://localhost:3000/api/v1/books/?q=OLID:OL6732939M' \
  -H 'Host: localhost:3000' \
```

Filter featured books by query [OLID | Full title | Partial title]

- Query parameter `q` needs to min of 2 chars.

```
curl -X GET \
  'http://localhost:3000/api/v1/books/?q=farewell to arms' \
  -H 'Host: localhost:3000' \
```

- Query parameter `q` for OLID can start with OLID

```
curl -X GET \
  'http://localhost:3000/api/v1/books/?q=OLID:OL6732939M' \
  -H 'Host: localhost:3000' \
```

- Query parameter `q` can be just the code

```
curl -X GET \
  'http://localhost:3000/api/v1/books/?q=OL6732939M' \
  -H 'Host: localhost:3000' \
```

# Docker

Build the image `sudo docker build -t rjmacarthy/express-typescript-starter .`

Run the image `docker-compose up`

Open `http://localhost:8080`

# License

MIT - Do with as you like.
